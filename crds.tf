data "curl" "cert_manager_crds_yaml" {
    http_method = "GET"
    uri = "https://github.com/cert-manager/cert-manager/releases/download/v1.7.1/cert-manager.crds.yaml"
}

data "kubectl_file_documents" "cert_manager_crds_docs" {
    content = tostring(data.curl.cert_manager_crds_yaml.response)
}

resource "kubectl_manifest" "cert_manager_crds" {
    for_each  = data.kubectl_file_documents.cert_manager_crds_docs.manifests
    yaml_body = each.value
}