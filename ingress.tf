resource "helm_release" "ingress_nginx" {
  name       = "ingress-nginx"
  repository = "https://kubernetes.github.io/ingress-nginx"
  chart      = "ingress-nginx"
  version    = "4.4.0"
  namespace  = kubernetes_namespace_v1.ingress_nginx_namespace.metadata.0.name
  timeout    = 300

  values = [<<EOF
controller:
  kind: DaemonSet
  hostNetwork: true
  dnsPolicy: ClusterFirstWithHostNet
  service:
    type: ""
EOF
  ]
}