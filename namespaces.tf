resource "kubernetes_namespace_v1" "ingress_nginx_namespace" {
  metadata {
    name = "ingress-nginx"
  }
}

resource "kubernetes_namespace_v1" "cattle_system_namespace" {
  metadata {
    name = "cattle-system"
  }
}

resource "kubernetes_namespace_v1" "cert_manager_namespace" {
  metadata {
    name = "cert-manager"
  }
}
