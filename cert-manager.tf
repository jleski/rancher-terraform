resource "helm_release" "cert_manager" {
  name       = "cert-manager"
  repository = "https://charts.jetstack.io"
  chart      = "cert-manager"
  version    = "1.7.1"
  namespace  = kubernetes_namespace_v1.cert_manager_namespace.id
  timeout    = 300

  depends_on = [
    kubectl_manifest.cert_manager_crds
  ]
}