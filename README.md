# Rancher Terraform

Tiny terraform script to install Rancher to Kubernetes.

## Description

This Terraform code will deploy NGINX Ingress Controller, Cert Manager and Rancher server using Helm.

Ingress Controller is using Host Networking, meaning that the host ports 443/tcp and 80/tcp must be unbound before deploying.

This setup is most suitable for single node deployments in VM environments outside public cloud.

## Requirements

Kubernetes installed and `kubectl` configured with `helm`.

## Usage

Copy `terraform.tfvars.example` to `terraform.tfvars` and make the necessary changes.

## Kubernetes

For reference, the testbed Kubernetes was installed on vanilla `Ubuntu 22.04` with:

```bash
curl -sSLf https://get.k0s.sh | sudo K0S_VERSION=v1.24.4+k0s.0 sh
sudo k0s install controller --single
sudo k0s start
```