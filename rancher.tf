variable "rancher_hostname" {
    type = string
}
variable "admin_password" {
    type = string
}

resource "helm_release" "rancher" {
  name       = "rancher"
  repository = "https://releases.rancher.com/server-charts/latest"
  chart      = "rancher"
  namespace  = kubernetes_namespace_v1.cattle_system_namespace.id
  timeout    = 300

  depends_on = [
    helm_release.cert_manager
  ]
  
  set {
    name = "hostname"
    value = var.rancher_hostname
  }
  set {
    name = "bootstrapPassword"
    value = var.admin_password
  }

  values = [<<EOF
replicas: 1
ingress:
  ingressClassName: "nginx"
EOF
  ]
}